<?php

use App\Http\Controllers\Auth\RegisterController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admincontroller;
use App\Http\Controllers\StudentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['verify'=> true]);

Route::group(['middleware' => 'Admin'], function () {  

Route::get('/admin', [AdminController::class ,"Index"])->name('admin');
Route::post('/submitroll', [AdminController::class ,"submit"])->name('submit');

});
Route::group(['middleware' => 'Manager'], function () {  
Route::get('/student', [StudentController::class ,'index'])->name('manager');
Route::post('/storerecord', [StudentController::class,'store']);
Route::delete('/deleterecord/{id}', [StudentController::class,'delete']);
Route::put('/updaterecord/{id}', [StudentController::class,'update']);
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware('User');




