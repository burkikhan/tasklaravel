<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use Illuminate\Http\Request;

class Admincontroller extends Controller
{
   
    public function index(){
        $user = User::all();
        return view('Admin.index')->with('users', $user);
    }
    public function submit(Request $request){
     
        DB::table('users')
            ->where('id', $request->id)
            ->update(['role' => $request->role]);
        $user = User::all();
        return view('Admin.index')->with('users', $user);
    }
}
