<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        $students = Student::all();
        return view('Manager.index')->with('students', $students);
    }

    public function store(Request $request)
    {
        // dd($request->all());

        $image = $request->file('image');
        $my_image = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('upload'), $my_image);

        $students = new Student;
        $students->name = $request->input('name');
        $students->email = $request->input('email');
        $students->address = $request->input('address');
        $students->image = $my_image;
        if ($students->save()) {
            return response()->json($students);
        }
    }
    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        $my_image = rand() . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('upload'), $my_image);

        $students = Student::find($id);
        $students->name = $request->input('name');
        $students->email = $request->input('email');
        $students->address = $request->input('address');
        $students->image = $my_image;
        if ($students->save()) {
            return response()->json($students);
        }
    }

    public function delete(Request $request, $id)
    {
        $students = Student::find($id);
        $students->delete();
        unlink("upload/" . $students['image']);
        return $students;
    }
}
