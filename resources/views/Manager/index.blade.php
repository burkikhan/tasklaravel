
@extends('layouts.app')
@section('content')

<!doctype html>
<html lang="en">
  <head>
    <title>CRUD</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
        <!-- Adding data Modal -->
        <div class="modal fade" id="studentaddmodel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="color:rgb(255, 255, 255); background-color: rgb(13, 189, 13);">
                <h5 class="modal-title" id="exampleModalLabel">Add User Data USing Ajax </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:rgb(255, 255, 255);">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            <form  id="addstudent" enctype="multipart/form-data" >
               
                <div class="modal-body" >
                  
                {{csrf_field()}}

                 <div class="form-group">
                   <label>User Name</label>
                   <input type="text" class="form-control" name="name" placeholder="Enter Your Name">
                 </div>

                 <div class="form-group">
                    <label>Email Address</label>
                    <input type="text" class="form-control" name="email" placeholder="Enter Your Email">
                  </div>

                  <div class="form-group">
                    <label>Home Address</label>
                    <input type="text" class="form-control" name="address" placeholder="Enter Your Address">
                  </div>

                  <div class="form-group">
                    <label>Image Upload</label>
                    <br>
                    <input type="file"    name="image"  >
                  </div>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
            </div>
            </div>
        </div>
           {{--End Add--}}

           <!-- Edit data Modal -->
           <div class="modal fade" id="studentEditModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header" style="color:rgb(255, 255, 255); background-color: rgb(13, 189, 13);">
                <h5 class="modal-title" id="exampleModalLabel">Edit User Data User Ajax </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:rgb(255, 255, 255);">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
            <form  id="editstudent" enctype="multipart/form-data" >
               
                <div class="modal-body">
                {{csrf_field()}}
                {{method_field('PUT')}}
                
                  <input type="hidden" name="id" id="id">
                  
                 <div class="form-group">
                  <label>User Name</label>
                  <input type="text" class="form-control" name="name" placeholder="Enter Your Name" id="name">
                </div>

                 <div class="form-group">
                    <label>Email Address</label>
                    <input type="text" class="form-control" name="email" placeholder="Enter Your Email" id="email">
                  </div>

                  <div class="form-group">
                    <label>Home Address</label>
                    <input type="text" class="form-control" name="address" placeholder="Enter Your Address" id="address">
                  </div>

                  <div class="form-group">
                    <label>Image Upload</label>
                    <br>
                    <input type="file"  name="image"  id="image">
                  </div>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Are You Sure</button>
                </div>
            </form>
            </div>
            </div>
        </div>
        {{--End of student edit model--}}

        {{--Delete student record--}}
        <div class="modal fade" id="studentdeleteModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
          <div class="modal-content">
              <div class="modal-header bg-danger" style="color:rgb(255, 255, 255);" >
              <h5 class="modal-title" id="exampleModalLabel">Delete User Data User Ajax </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="color:rgb(255, 255, 255);">
                  <span aria-hidden="true">&times;</span>
              </button>
              </div>
          <form  id="deleteFormID"  >
             
              <div class="modal-body">
              {{csrf_field()}}
              {{method_field('delete')}}
              
                <input type="hidden" name="id" id="delete_id">
                <p>Are Your You Want To Delete The Data ?</p>
              </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-danger">Delete Student Record</button>
              </div>
          </form>
          </div>
          </div>
      </div>
        {{--end delete student record--}}

        <div class="container">
            <div class="pt-5">
                <div class="row d-flex justify-content-center align-items-center">
                    <h1>Laravel CRUD - AJAX Jquery Bootstrap</h1>
                </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#studentaddmodel">
                        Student Add Data
                    </button>
                    
           {{--Table data display--}}         
           
                </div>
                <br>
                <br>
                <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Image</th>
                        <th scope="col">&nbsp;Edit&nbsp;&nbsp;&nbsp;&nbsp;Delete</th>
                      </tr>
                    </thead>
                    <tbody id="displaydata">
                      @foreach ($students ?? '' as $student)
                      <tr id="row{{$student->id}}">
                        <td>{{$student->id}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->email}}</td>
                        <td>{{$student->address}}</td>
                        <td><img src="{{ asset('upload') }}/{{$student->image}}" alt="" style="width: 50px;height:50px;"></td>
                        <td>
                            <button class="btn btn-success editbtn">Edit</button>
                            <button class="btn btn-danger deletebtn">Delete</button>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
      

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <!-- get value here -->
     <script>
      $(document).ready(function(){
        $('.editbtn').on('click', function(){
          $('#studentEditModal').modal('show');
          $tr =$(this).closest('tr');
          var data = $tr.children("td").map(function(){
            return $(this).text();}).get();
          $('#id').val(data[0]);
          $('#name').val(data[1]);
          $('#email').val(data[2]);
          $('#address').val(data[3]);
        });



        $('#editstudent').on('submit', function(e){
            e.preventDefault();
            
            var id = $('#id').val();
            var form = $('#editstudent')[0];
            var formData = new FormData(form);
            formData.append('_method', 'put');

        
            $.ajax({
              type: "POST",
              url: "/updaterecord/"+id,
              data: formData,
              processData: false,
              contentType: false,
              success: function(data){
                console.log(data.id);
                     $('#row' + data.id).replaceWith(`
                     <tr id="row`+data.id+`">
                        <td>`+ data.id + `</td>
                        <td>` + data.name + `</td>
                        <td>` + data.email + `</td>
                        <td>` + data.address + `</td>
                        <td><img src="http://127.0.0.1:8000/upload/`+ data.image + `" style="width:50px;height:50px;" alt=""</td>
                        <td>
                            <button class="btn btn-success ajaxEditBtn">Edit</button>
                            <button class="btn btn-danger ajaxDeleteBtn">Delete</button>
                        </td>
                      </tr>`)
                      $("#editstudent").get(0).reset();
                $('#studentEditModal').modal('hide');
              },
              error: function(error){
                console.log(error);
              }
            });
        });
      });

      $(document).on("click",".ajaxEditBtn", function(){
        $('#studentEditModal').modal('show');
          $tr =$(this).closest('tr');
          var data = $tr.children("td").map(function(){
            return $(this).text();}).get();
          $('#id').val(data[0]);
          $('#name').val(data[1]);
          $('#email').val(data[2]);
          $('#address').val(data[3]);
          
         
           
      });
    </script> 

    {{--Delete record--}}

    <script type="text/javascript">
    $(document).ready(function(){
      $('.deletebtn').on('click', function(){
      $('#studentdeleteModel').modal('show');
      $tr =$(this).closest('tr');
          var data = $tr.children("td").map(function(){
            return $(this).text();}).get();
          $('#delete_id').val(data[0]);
      });

      
      $('#deleteFormID').on('submit', function(e){
            e.preventDefault();
            var id = $('#delete_id').val();
            $.ajax({
              type: "DELETE",
              url: "/deleterecord/"+id,
              data: $('#deleteFormID').serialize(),
              success: function(data){
                console.log(data);
                $('#row' + data.id).remove();
                $('#studentdeleteModel').modal('hide');
                alert("data delete");

                },
                error: function(error){
                  console.log(error);
                }

              });
            });
          });

          $(document).on("click",".ajaxDeleteBtn", function(){
        $('#studentdeleteModel').modal('show');
        $tr =$(this).closest('tr');
          var data = $tr.children("td").map(function(){
            return $(this).text();}).get();
          $('#delete_id').val(data[0]);
       
         
           
      });
    </script>
    <script type="text/javascript">
    $(document).ready(function (){
        $('#addstudent').on('submit', function(e){
            e.preventDefault();
          
            var form = $('#addstudent')[0];
            var formData = new FormData(form);
            $.ajax({
                
                type: "POST",
                url: "/storerecord",
                data: formData,
                processData: false,
                contentType: false,

                success: function (data){
                     console.log(data);
                     $('#displaydata').append(`
                     <tr id="row`+data.id+`">
                        <td>`+ data.id + `</td>
                        <td>` + data.name + `</td>
                        <td>` + data.email + `</td>
                        <td>` + data.address + `</td>
                        <td><img src="http://127.0.0.1:8000/upload/`+ data.image + `" style="width:50px;height:50px;" alt=""></td>
                        <td>
                            <button class="btn btn-success ajaxEditBtn">Edit</button>
                            <button class="btn btn-danger ajaxDeleteBtn">Delete</button>
                        </td>
                      </tr>`)
                      $("#addstudent").get(0).reset();
                     $('#studentaddmodel').modal('hide');
                     $('.modal-backdrop.show').removeClass();
                 
                },
                error: function (error){
                    
                    alert("data not saved error occur");
                }
            });
        });
    });
        
    
        
    </script>
   
   
  </body>
</html>
@endsection