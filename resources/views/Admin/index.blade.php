
@extends('layouts.app')

@section('content')

<div class="container mt-5">

                    <table class="table table-dark">
                    <thead>
                      <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Role</th>
                        <th scope="col">Grant Role</th>
                      </tr>
                    </thead>
                    <tbody id="displaydata">
                  
                      @foreach ($users as $user)
                      <tr id="row{{$user->id}}">
                        <td>{{$user->id}}</td>
                        <td>{{$user->role}}</td>
                        <td>
                        <form method="post" action="{{ route('submit')}}">
                        {{csrf_field()}}
                            <input type="hidden" name="id" value="{{$user->id}}">
                             <select id="Roll no" name='role'>
                                 <option value="1">Admin</option>
                                 <option value="2">Manager</option>
                                  <option value="3">User</option>
                                </select>
                                &nbsp    &nbsp    &nbsp
                                <button type="submit" class="btn btn-primary"> Change</button>
                         </form>
                            </td>
                           
   
                      </tr>
                      @endforeach
                    
                    </tbody>
                  </table>

                    </div>



@endsection